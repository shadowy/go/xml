all: test lint-full

test:
	@echo ">> test"
	@go test ./... -coverprofile cover.out
	@go tool cover -html=cover.out -o cover.html

lint-full: lint card

lint-badge-full: lint card card-badge

card:
	@echo ">> card"

lint:
	@echo ">> lint"

card-badge:
	@echo ">>card-badge"
