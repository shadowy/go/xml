<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.2"></a>
## [v1.0.2] - 2019-10-16
### Bug Fixes
- fix issue with tag with namespace


<a name="v1.0.1"></a>
## v1.0.1 - 2019-08-28
### Features
- correct work with namespace


[Unreleased]: https://gitlab.com/shadowy/go/xml/compare/v1.0.2...HEAD
[v1.0.2]: https://gitlab.com/shadowy/go/xml/compare/v1.0.1...v1.0.2
